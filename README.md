~~~~~~~~7/15/2018~~~~~~~~~
This script is designed to pull data from the U.S. Bureau of Labor Statistics
within the time range specified, and place all data into a csv, file.
It will return an error if the data does not exist. Data is stored in the
same directory the script is located. Files are named after the name of the
series. 

The bls.conf file holds your api key, if you registered to recieve one. Leave
'api_key' entry blank if you do not have one.

Example of use: 

The following will return data from 2000 to 2018 for each series. 

./api.py -start 2000 -end 2018 -series SERIES1 SERIES2 SERIES3


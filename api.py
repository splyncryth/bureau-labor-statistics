import requests
import json
import csv
import sys
import os.path
import argparse
import configparser

class dataseries(object):
 def __init__(self):
  pass

 def __getData(self,start,end,series):
  try:
   config=configparser.ConfigParser()
   config.read('./bls.conf')
   api_key=config.get("configuration","api_key")
   headers = {'Content-type': 'application/json'}
   data = json.dumps({"seriesid":[series],"startyear":start, "endyear":end,"registrationkey":str(api_key.strip('"'))})
   p = requests.post('https://api.bls.gov/publicAPI/v2/timeseries/data/', data=data, headers=headers)
   out = json.loads(p.text)
   if out['status'] != "REQUEST_SUCCEEDED":
    self.__error(out['status'],out['message'][0])
   return out
  except Exception as e:
   self.__error(e)

 def printData(self,start,end,series):
  data = self.__getData(start,end,series)
  if not data['Results']['series'][0]['data']:
   self.__error(data['message'])
  if data['message']:
   for line in data['message']:
    print(line)
  last = 0
  if os.path.isfile(series + '.csv'):
   outputFile = open(series + '.csv', 'a')
   output = csv.writer(outputFile)
  else:
   outputFile = open(series + '.csv', 'w')
   output = csv.writer(outputFile)
   output.writerow(data['Results']['series'][0]['data'][0].keys())

  try:
   for row in data['Results']['series'][0]['data']:
    year = row['year']
    output.writerow(row.values())
    if int(year) > last:
      last = int(year)
   outputFile.close()
   if last != end:
    last = int(last) + 1
    self.printData(last,end,series)
  except Exception as e:
   self.__error(e)

 def __error(self,*args):
  for line in args:
   if type(line) is list:
    for data in line:
     print(data)
   else:
    print(line)
  sys.exit(2)

def main():
 try:
  parser = argparse.ArgumentParser()
  parser.add_argument('-start', action="store",type=int, dest="start", required=True)
  parser.add_argument('-end', action="store",type=int, dest="end", required=True)
  parser.add_argument('-series',nargs='+', action="store",type=str, dest="series", required=True)
  args = vars(parser.parse_args())
  getdata=dataseries()
  for series in args['series']:
   getdata.printData(args['start'],args['end'],series)
 except SystemExit:
   sys.exit(3)
 except Exception as e:
   print(e)
   sys.exit(2)

if __name__ == "__main__":
 main()
